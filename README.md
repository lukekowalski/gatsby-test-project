## Run the build locally
- `npm install`
- `npm start`
- open `localhost:8000` in your browser



## Create a production bundle

- `npm run build`
- Production ready files will be available in the `public` folder