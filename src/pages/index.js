import * as React from "react";
import JSONData from "../content/homepage.json"

import "../style/global.css";

const IndexPage = () => {
  const {title, description, button } = JSONData;
  return (
    <>
    <div className="test-container">
      <h1>{title}</h1>
      <p>{description}</p>
      <a href={button.target}>{button.anchor}</a>
    </div>
    </>
  );
};

export default IndexPage;
